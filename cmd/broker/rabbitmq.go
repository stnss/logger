package broker

import (
	"flag"
	"fmt"
	"gitlab.com/djelasid/neo-logger/internal/bootstrap"
	"gitlab.com/djelasid/neo-logger/internal/controller"
	"gitlab.com/djelasid/neo-logger/pkg/config"
	"gitlab.com/djelasid/neo-logger/pkg/logger"
	"os"
	"strings"
)

var (
	flags  = flag.NewFlagSet("rabbit", flag.ExitOnError)
	name   = flags.String("name", "", "queue and exchange name")
	topics = flags.String("topics", "", "topic to subscribes")
	help   = flags.Bool("guide", false, "Print Help")
)

func ServeRabbitMQ() {
	flags.Usage = usage
	_ = flags.Parse(os.Args[2:])

	args := flags.Args()

	if (len(args) == 0 && (*name == "" || *topics == "")) || *help {
		flags.Usage()
		return
	}

	var topicList []string
	for _, t := range strings.Split(*topics, "|") {
		if t != "" {
			topicList = append(topicList, t)
		}
	}

	logger.SetJSONFormatter()
	cfg, err := config.LoadAllConfigs()
	if err != nil {
		logger.Fatal(fmt.Sprintf("Failed to load configuration file: %v", err))
	}

	bootstrap.RegistryLogger(cfg)
	db := bootstrap.RegistryMongoDB(cfg)
	registryOpenTelemetry, err := bootstrap.RegistryOpenTelemetry(cfg)
	if err != nil {
		_ = registryOpenTelemetry.Close()
		return
	}

	mController := controller.NewMessageController(db)

	subs := bootstrap.RegistryRabbitMQSubscriber(*name, cfg, mController)
	if err != nil {
		logger.Fatal(fmt.Sprintf("Failed connect to RabbitMQ Cause: %v", err))
	}

	err = subs.Listen(topicList)

	if err != nil {
		logger.Fatal(fmt.Sprintf("Failed to Listen Topic Cause: %v", err))
	}
}

func usage() {
	fmt.Printf("%v\n", usageCommands)
}

var (
	usageCommands = `
	Usage:
		go run main.go rabbit [flags]

	Flags:
	--name 		string    	name of queue and exchange
	--topics 	string		separate with pipeline "|" if want multiple bindings
  	--guide          		print help
`
)
