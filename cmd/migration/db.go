package migration

import (
	"fmt"
	"gitlab.com/djelasid/neo-logger/pkg/database/mysql"

	"gitlab.com/djelasid/neo-logger/pkg/config"
	"gitlab.com/djelasid/neo-logger/pkg/logger"
)

func MigrateDatabase() {
	cfg, err := config.LoadAllConfigs()

	if err != nil {
		logger.Fatal(fmt.Sprintf("Failed to load configuration file: %v", err))
	}

	mysql.DatabaseMigration(cfg)
}
