package mongodb

import (
	"context"
	"fmt"
	"gitlab.com/djelasid/neo-logger/pkg/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

func connect(cfg *config.Config) (*mongo.Database, error) {
	databaseConfig := cfg.MongoDBConfig
	url := fmt.Sprintf("mongodb://%v:%v", databaseConfig.DBHost, databaseConfig.DBPort)
	opts := options.Client().
		ApplyURI(url).
		SetAuth(options.Credential{
			AuthSource:  "admin",
			Username:    databaseConfig.DBUser,
			Password:    databaseConfig.DBPassword,
			PasswordSet: true,
		}).
		SetMaxConnIdleTime(time.Duration(databaseConfig.MaxIdleTime) * time.Hour).
		SetConnectTimeout(time.Duration(databaseConfig.MaxConnLifetime) * time.Hour).
		SetMaxConnecting(uint64(databaseConfig.MaxOpenConn))

	tlsConfig, err := databaseConfig.TlsConfig(cfg.AppEnv)
	if err != nil {
		return nil, err
	}

	if tlsConfig != nil {
		opts.SetTLSConfig(tlsConfig)
	}

	ctx := context.TODO()

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	return client.Database(databaseConfig.DBName), nil
}

func Connect(cfg *config.Config) (*mongo.Database, error) {
	return connect(cfg)
}
