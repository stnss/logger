package util

import (
	"encoding/json"
	"errors"
	"reflect"
)

func ToMap(data any) (map[string]any, error) {
	dataType := reflect.TypeOf(data)

	if dataType.Kind() != reflect.Struct {
		return nil, errors.New("only accept struct data type")
	}

	b, _ := json.Marshal(&data)
	var result map[string]any
	if err := json.Unmarshal(b, &result); err != nil {
		return nil, err
	}

	return result, nil
}
