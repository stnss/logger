FROM golang:1.21 AS builder

WORKDIR /build

COPY . .

RUN go mod tidy && go mod download && go mod vendor

ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -ldflags="-s -w" -o logger .

FROM scratch

COPY --from=builder /build/logger /
COPY --from=builder /build/config /config

EXPOSE 3001