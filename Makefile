BINARY_NAME=skeleton
build:
	@go build -o bin/${BINARY_NAME} main.go

http:
	@./bin/${BINARY_NAME} http

log-trans:
	@./bin/${BINARY_NAME} rabbit --name logger_transaction --topics="event.log.transaction"

log-usage:
	@./bin/${BINARY_NAME} rabbit --name logger_usage --topics="event.log.usage"

install:
	@echo "Installing dependencies...."
	@rm -rf vendor
	@rm -f Gopkg.lock
	@rm -f glide.lock
	@go mod tidy && go mod download && go mod vendor

start-http:
	@go run main.go http

start-rabbit:
	@go run main.go rabbit

migrate-create:
	@./bin/${BINARY_NAME} db:migrate create $(NAME) sql

migrate-up:
	@./bin/${BINARY_NAME} db:migrate up

migrate-down:
	@./bin/${BINARY_NAME} db:migrate up

migrate-status:
	@./bin/${BINARY_NAME} db:migrate status