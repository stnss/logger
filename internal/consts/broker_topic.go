package consts

const (
	// EventLogAny const
	EventLogAny = "event.log.#"

	// EventLogTransaction const
	EventLogTransaction = "event.log.transaction"

	// EventLogUsage const
	EventLogUsage = "event.log.usage"
)
