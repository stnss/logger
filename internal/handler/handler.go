package handler

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/djelasid/neo-logger/internal/appctx"
	"gitlab.com/djelasid/neo-logger/internal/controller/contract"
	"gitlab.com/djelasid/neo-logger/pkg/config"
)

func HttpRequest(xCtx *fiber.Ctx, svc contract.Controller, conf *config.Config) appctx.Response {
	data := appctx.Data{
		FiberCtx: xCtx,
		Cfg:      conf,
	}
	return svc.Serve(data)
}
