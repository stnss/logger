package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/djelasid/neo-logger/internal/appctx"
	"gitlab.com/djelasid/neo-logger/internal/controller"
	"gitlab.com/djelasid/neo-logger/internal/controller/contract"
	"gitlab.com/djelasid/neo-logger/internal/handler"
	"gitlab.com/djelasid/neo-logger/internal/middleware"
	"gitlab.com/djelasid/neo-logger/pkg/config"
)

type router struct {
	cfg   *config.Config
	fiber fiber.Router
}

func (rtr *router) handle(hfn httpHandlerFunc, svc contract.Controller, mdws ...middleware.MiddlewareFunc) fiber.Handler {
	return func(xCtx *fiber.Ctx) error {

		//check registered middleware functions
		if rm := middleware.FilterFunc(rtr.cfg, xCtx, mdws); rm.Code != fiber.StatusOK {
			// return response base on middleware
			res := *appctx.NewResponse().
				WithCode(rm.Code).
				WithError(rm.Errors).
				WithMessage(rm.Message)
			return rtr.response(xCtx, res)
		}

		//send to controller
		resp := hfn(xCtx, svc, rtr.cfg)
		return rtr.response(xCtx, resp)
	}
}

func (rtr *router) response(fiberCtx *fiber.Ctx, resp appctx.Response) error {
	fiberCtx.Set(fiber.HeaderContentType, fiber.MIMEApplicationJSON)
	return fiberCtx.Status(resp.Code).Send(resp.Byte())
}

func (rtr *router) Route() {
	//init db
	//db := bootstrap.RegistryMongoDB(rtr.cfg)

	//define repositories

	//define services

	//define middleware

	//define provider

	//define controller

	health := controller.NewGetHealth()

	rtr.fiber.Get("/ping", rtr.handle(
		handler.HttpRequest,
		health,
	))

}

func NewRouter(cfg *config.Config, fiber fiber.Router) Router {
	return &router{
		cfg:   cfg,
		fiber: fiber,
	}
}
