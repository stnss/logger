package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/djelasid/neo-logger/internal/appctx"
	"gitlab.com/djelasid/neo-logger/internal/controller/contract"
	"gitlab.com/djelasid/neo-logger/pkg/config"
)

type httpHandlerFunc func(xCtx *fiber.Ctx, svc contract.Controller, conf *config.Config) appctx.Response

type Router interface {
	Route()
}
