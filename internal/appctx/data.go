package appctx

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/djelasid/neo-logger/pkg/config"
)

type Data struct {
	FiberCtx *fiber.Ctx
	Cfg      *config.Config
}
