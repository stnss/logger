package service

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/djelasid/neo-logger/internal/entity"
	"gitlab.com/djelasid/neo-logger/internal/repositories"
	"gitlab.com/djelasid/neo-logger/pkg/broker"
	"gitlab.com/djelasid/neo-logger/pkg/logger"
	"gitlab.com/djelasid/neo-logger/pkg/tracer"
	"gitlab.com/djelasid/neo-logger/pkg/util"
)

type logUsageServiceImpl struct {
	repo repositories.LogUsageRepository
}

func NewLogUsageService(repo repositories.LogUsageRepository) LogService {
	return &logUsageServiceImpl{repo: repo}
}

func (service *logUsageServiceImpl) Logging(ctx context.Context, payload broker.MessagePayload) error {
	var (
		lf   = logger.NewFields(logger.EventName("logUsageServiceProcess"))
		data entity.UsageLog
	)

	ctx, span := tracer.NewSpan(ctx, "UsageLogService.Logging", nil)
	defer span.End()

	stringData, err := json.Marshal(payload.Data)
	if err != nil {
		tracer.AddSpanError(span, err)
		return err
	}

	if err = json.Unmarshal(stringData, &data); err != nil {
		tracer.AddSpanError(span, err)
		return err
	}

	dataMap, err := util.ToMap(data)

	if err != nil {
		tracer.AddSpanError(span, err)
		return err
	}

	err = service.repo.Store(ctx, dataMap)

	if err != nil {
		tracer.AddSpanError(span, err)
		logger.ErrorWithContext(ctx, fmt.Sprintf("store log usage got error: %v", err), lf...)
		return err
	}

	return nil
}
