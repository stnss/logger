package service

import (
	"context"
	"gitlab.com/djelasid/neo-logger/pkg/broker"
)

type LogService interface {
	Logging(ctx context.Context, payload broker.MessagePayload) error
}
