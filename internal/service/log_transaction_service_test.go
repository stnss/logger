package service

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/djelasid/neo-logger/internal/entity"
	"gitlab.com/djelasid/neo-logger/mocks/repositories"
	"gitlab.com/djelasid/neo-logger/pkg/broker"
	"math/rand"
	"testing"
	"time"
)

func TestLogTransactionService_Logging(t *testing.T) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	testCases := []struct {
		testName     string
		payload      broker.MessagePayload
		expectResult any
		expectRepo   any
	}{
		{
			testName: "[TEST] Success Store Transaction Log",
			payload: broker.MessagePayload{
				Type:    "topup",
				Message: "success topup",
				Data: &entity.TransactionLogRequest{
					ExternalId:    "testing-external",
					ServiceName:   "demografi",
					LogType:       "topup",
					IpAddress:     "127.0.0.1",
					UserAgent:     "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/118.0",
					VendorName:    "dukcapil",
					CauserId:      uuid.New(),
					CauserName:    "Topup User",
					SubjectId:     uuid.New(),
					SubjectName:   "TekenAja Unit Test",
					TokenAffected: rand.Intn(5-1+1) + 1,
					CreatedAt:     time.Now(),
				},
			},
			expectRepo:   nil,
			expectResult: nil,
		},
		{
			testName: "[TEST] Failed store transaction log (Database issue)",
			payload: broker.MessagePayload{
				Type:    "topup",
				Message: "success topup",
				Data:    nil,
			},
			expectRepo:   errors.New("failed to create new mongo document"),
			expectResult: errors.New("failed to create new mongo document"),
		},
	}

	for _, test := range testCases {
		var (
			repo = new(repositories.MockLogTransactionRepository)
		)

		t.Run(test.testName, func(t *testing.T) {
			repo.On("Store", mock.Anything, mock.Anything).
				Return(test.expectRepo)

			actual := NewLogTransactionService(repo).
				Logging(ctx, test.payload)

			assert.Equal(
				t,
				test.expectResult,
				actual,
			)
		})
	}
}
