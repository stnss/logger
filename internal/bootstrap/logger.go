package bootstrap

import (
	"gitlab.com/djelasid/neo-logger/pkg/config"
	"gitlab.com/djelasid/neo-logger/pkg/logger"
	"gitlab.com/djelasid/neo-logger/pkg/util"
)

func RegistryLogger(cfg *config.Config) {
	loggerConfig := logger.Config{
		Environment: util.EnvironmentTransform(cfg.AppEnv),
		Debug:       cfg.AppDebug,
		Level:       cfg.LogLevel,
		ServiceName: cfg.AppName,
	}

	logger.Setup(loggerConfig)

	if cfg.LogLoki {
		logger.AddHook(cfg.LoggerConfig.WithLokiHook(cfg))
	}
}
