package bootstrap

import (
	"fmt"
	"gitlab.com/djelasid/neo-logger/pkg/config"
	"gitlab.com/djelasid/neo-logger/pkg/database/mongodb"
	"gitlab.com/djelasid/neo-logger/pkg/database/mysql"
	"gitlab.com/djelasid/neo-logger/pkg/logger"
)

func RegistryDatabase(cfg *config.Config) *mysql.DB {
	// Remove this code below if no need database
	db, err := mysql.ConnectDatabase(cfg)
	if err != nil {
		logger.Fatal(fmt.Sprintf("Failed to connect to database: %v", err))
	}

	return mysql.New(db, false, cfg.DatabaseConfig.DBName)
}

func RegistryMongoDB(cfg *config.Config) *mongodb.DB {
	db, err := mongodb.Connect(cfg)
	if err != nil {
		logger.Fatal(fmt.Sprintf("Failed to connect to database: %v", err))
	}

	return mongodb.New(db, false, cfg.DatabaseConfig.DBName)
}
