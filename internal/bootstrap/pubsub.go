package bootstrap

import (
	"context"
	"fmt"
	"gitlab.com/djelasid/neo-logger/pkg/config"
	"gitlab.com/djelasid/neo-logger/pkg/logger"
	"gitlab.com/djelasid/neo-logger/pkg/pubsubx"

	"cloud.google.com/go/pubsub"
	"google.golang.org/api/option"
)

func RegistryPubSubConsumer(cfg *config.Config) pubsubx.Subscriberer {
	credOpt := option.WithCredentialsFile(cfg.GCPConfig.PubsubAccountPath)
	cl, err := pubsub.NewClient(context.Background(), cfg.GCPConfig.ProjectID, credOpt)
	if err != nil {
		logger.Fatal(fmt.Sprintf("google pusbsub conusmer error:%v", err))
	}

	return pubsubx.NewGSubscriber(cl)
}

func RegistryPubSubPublisher(cfg *config.Config) pubsubx.Publisher {
	credOpt := option.WithCredentialsFile(cfg.GCPConfig.PubsubAccountPath)
	cl, err := pubsub.NewClient(context.Background(), cfg.GCPConfig.ProjectID, credOpt)
	if err != nil {
		logger.Fatal(fmt.Sprintf("google pusbsub publisher error:%v", err))
	}

	return pubsubx.NewGPublisher(cl)
}
