package bootstrap

import (
	"gitlab.com/djelasid/neo-logger/internal/controller/contract"
	"gitlab.com/djelasid/neo-logger/pkg/broker"
	"gitlab.com/djelasid/neo-logger/pkg/config"
	"gitlab.com/djelasid/neo-logger/pkg/logger"
)

func RegistryRabbitMQSubscriber(name string, cfg *config.Config, mController contract.MessageController) broker.Subscriber {
	conn, err := broker.ConnectRabbitMQ(cfg)
	if err != nil {
		logger.Fatal(err)
	}
	return broker.NewSubscriber(conn, name, mController)
}

func RegistryRabbitMQPublisher(name string, cfg *config.Config) broker.Publisher {
	conn, err := broker.ConnectRabbitMQ(cfg)
	if err != nil {
		logger.Fatal("dial rabbit mq failed")
	}

	return broker.NewPublisher(conn)
}
