package controller

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/djelasid/neo-logger/internal/consts"
	"gitlab.com/djelasid/neo-logger/internal/controller/contract"
	"gitlab.com/djelasid/neo-logger/internal/repositories"
	"gitlab.com/djelasid/neo-logger/internal/service"
	"gitlab.com/djelasid/neo-logger/pkg/broker"
	"gitlab.com/djelasid/neo-logger/pkg/database/mongodb"
	"gitlab.com/djelasid/neo-logger/pkg/logger"
	"gitlab.com/djelasid/neo-logger/pkg/tracer"
)

type messageControllerImpl struct {
	db mongodb.Adapter
}

func (controller messageControllerImpl) Serve(data amqp.Delivery) error {
	var (
		svc     service.LogService
		payload broker.MessagePayload
	)

	ctx, span := tracer.NewSpan(context.Background(), "MessageControllerImpl.Serve", nil)
	defer span.End()

	if err := json.Unmarshal(data.Body, &payload); err != nil {
		tracer.AddSpanError(span, err)
		err = errors.Wrap(err, "failed to unmarshal data payload")
		logger.ErrorWithContext(ctx, err)
		return err
	}

	switch data.RoutingKey {
	case consts.EventLogTransaction:
		repo := repositories.NewLogTransactionRepositoryImpl(controller.db)
		svc = service.NewLogTransactionService(repo)
	case consts.EventLogUsage:
		repo := repositories.NewLogUsageRepositoryImpl(controller.db)
		svc = service.NewLogUsageService(repo)
	default:
		return nil
	}

	if err := svc.Logging(ctx, payload); err != nil {
		tracer.AddSpanError(span, err)
		err = errors.Wrap(err, "failed to process log")
		logger.ErrorWithContext(ctx, err)
		return err
	}

	logger.Info("Success process data from broker")

	return nil
}

func NewMessageController(db mongodb.Adapter) contract.MessageController {
	return &messageControllerImpl{
		db: db,
	}
}
