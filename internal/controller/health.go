package controller

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/djelasid/neo-logger/internal/appctx"
	"gitlab.com/djelasid/neo-logger/internal/controller/contract"
)

type getHealth struct {
}

func (g *getHealth) Serve(xCtx appctx.Data) appctx.Response {
	// Ping Endpoint
	return *appctx.NewResponse().WithCode(fiber.StatusOK).WithMessage("ok").WithData(struct {
		Message string `json:"message"`
	}{
		Message: "Waras!",
	})
}

func NewGetHealth() contract.Controller {
	return &getHealth{}
}
