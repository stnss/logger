package contract

import (
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/djelasid/neo-logger/internal/appctx"
)

type MessageController interface {
	Serve(data amqp091.Delivery) error
}

type Controller interface {
	Serve(xCtx appctx.Data) appctx.Response
}
