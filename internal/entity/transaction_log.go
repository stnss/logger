package entity

import (
	"github.com/google/uuid"
	"time"
)

type TransactionLog struct {
	Id            string    `json:"id" db:"id" bson:"_id"`
	ExternalId    string    `json:"external_id" db:"external_id" bson:"external_id,omitempty"`
	ServiceName   string    `json:"service_name" db:"service_name" bson:"service_name"`
	LogType       string    `json:"log_type" db:"log_type" bson:"log_type"`
	IpAddress     string    `json:"ip_address" db:"ip_address" bson:"ip_address"`
	UserAgent     string    `json:"user_agent" db:"user_agent" bson:"user_agent"`
	VendorName    string    `json:"vendor_name" db:"vendor_name" bson:"vendor_name"`
	CauserId      string    `json:"causer_id" db:"causer_id" bson:"causer_id"`
	CauserName    string    `json:"causer_name" db:"causer_name" bson:"causer_name"`
	SubjectId     string    `json:"subject_id,omitempty" db:"subject_id" bson:"subject_id,omitempty"`
	SubjectName   string    `json:"subject_name,omitempty" db:"subject_name" bson:"subject_name,omitempty"`
	TokenAffected int       `json:"token_affected" db:"token_affected" bson:"token_affected"`
	CreatedAt     time.Time `json:"created_at" db:"created_at" bson:"created_at"`
}

type TransactionLogRequest struct {
	ExternalId    string    `json:"external_id" bson:"external_id,omitempty"`
	ServiceName   string    `json:"service_name" bson:"service_name"`
	LogType       string    `json:"log_type" bson:"log_type"`
	IpAddress     string    `json:"ip_address" bson:"ip_address"`
	UserAgent     string    `json:"user_agent" bson:"user_agent"`
	VendorName    string    `json:"vendor_name" bson:"vendor_name"`
	CauserId      uuid.UUID `json:"causer_id" bson:"causer_id"`
	CauserName    string    `json:"causer_name" bson:"causer_name"`
	SubjectId     uuid.UUID `json:"subject_id,omitempty" bson:"subject_id,omitempty"`
	SubjectName   string    `json:"subject_name,omitempty" bson:"subject_name,omitempty"`
	TokenAffected int       `json:"token_affected" bson:"token_affected"`
	CreatedAt     time.Time `json:"created_at" bson:"created_at"`
}
