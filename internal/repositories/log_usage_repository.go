package repositories

import (
	"context"
	"gitlab.com/djelasid/neo-logger/internal/entity"
)

type LogUsageRepository interface {
	GetAll(ctx context.Context, filter map[string]any) ([]*entity.UsageLog, error)
	GetById(ctx context.Context, id string) (*entity.UsageLog, error)
	Store(ctx context.Context, data map[string]any) (err error)
}
