package repositories

import (
	"context"
	"gitlab.com/djelasid/neo-logger/internal/entity"
)

type LogTransactionRepository interface {
	GetAll(ctx context.Context, filter map[string]any) ([]*entity.TransactionLog, error)
	GetById(ctx context.Context, id string) (*entity.TransactionLog, error)
	Store(ctx context.Context, data map[string]any) (err error)
}
