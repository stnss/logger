package repositories

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gitlab.com/djelasid/neo-logger/internal/entity"
	"gitlab.com/djelasid/neo-logger/pkg/database/mongodb"
	"gitlab.com/djelasid/neo-logger/pkg/tracer"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type logTransactionRepositoryImpl struct {
	db mongodb.Adapter
}

func NewLogTransactionRepositoryImpl(db mongodb.Adapter) LogTransactionRepository {
	return &logTransactionRepositoryImpl{db: db}
}

func (repo *logTransactionRepositoryImpl) GetAll(ctx context.Context, filter map[string]any) ([]*entity.TransactionLog, error) {
	ctx, span := tracer.NewSpan(ctx, "LogTransactionRepositoryImpl.GetAll", nil)
	defer span.End()

	opts := options.Find().
		SetSort(bson.D{
			{"created_at", -1},
		})

	coll := repo.db.Collection("log_transaction")
	cursor, err := coll.Find(ctx, filter, opts)
	if err != nil {
		err = errors.Wrap(err, "failed to get data")
		tracer.AddSpanError(span, err)
		return []*entity.TransactionLog{}, err
	}

	var results []*entity.TransactionLog
	if err = cursor.All(context.TODO(), &results); err != nil {
		err = errors.Wrap(err, "failed to mapping results data")
		tracer.AddSpanError(span, err)
		return []*entity.TransactionLog{}, err
	}

	return results, nil
}

func (repo *logTransactionRepositoryImpl) GetById(ctx context.Context, id string) (*entity.TransactionLog, error) {
	ctx, span := tracer.NewSpan(ctx, "LogTransactionRepositoryImpl.GetById", nil)
	defer span.End()

	var transactionLog entity.TransactionLog
	coll := repo.db.Collection("log_transaction")
	if err := coll.FindOne(ctx, bson.D{
		{"_id", id},
	}).Decode(&transactionLog); err != nil {
		err = errors.Wrap(err, fmt.Sprintf("failed to get data with id: %v", id))
		tracer.AddSpanError(span, err)
		return nil, err
	}

	return &transactionLog, nil
}

func (repo *logTransactionRepositoryImpl) Store(ctx context.Context, data map[string]any) (err error) {
	ctx, span := tracer.NewSpan(ctx, "LogTransactionRepositoryImpl.Store", nil)
	defer span.End()

	data["_id"] = uuid.NewString()

	coll := repo.db.Collection("log_transaction")
	_, err = coll.InsertOne(ctx, data)
	if err != nil {
		tracer.AddSpanError(span, err)
		return errors.Wrap(err, "failed to create new mongo document")
	}

	return nil
}
