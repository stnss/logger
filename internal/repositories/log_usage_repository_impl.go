package repositories

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gitlab.com/djelasid/neo-logger/internal/entity"
	"gitlab.com/djelasid/neo-logger/pkg/database/mongodb"
	"gitlab.com/djelasid/neo-logger/pkg/tracer"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type logUsageRepositoryImpl struct {
	db mongodb.Adapter
}

func NewLogUsageRepositoryImpl(db mongodb.Adapter) LogUsageRepository {
	return &logUsageRepositoryImpl{db: db}
}

func (repo *logUsageRepositoryImpl) GetAll(ctx context.Context, filter map[string]any) ([]*entity.UsageLog, error) {
	ctx, span := tracer.NewSpan(ctx, "LogUsageRepositoryImpl.GetAll", nil)
	defer span.End()

	opts := options.Find().SetSort(bson.D{
		{"created_at", -1},
	})

	coll := repo.db.Collection("log_usage")
	cursor, err := coll.Find(ctx, filter, opts)
	if err != nil {
		err = errors.Wrap(err, "failed to get data")
		tracer.AddSpanError(span, err)
		return []*entity.UsageLog{}, err
	}

	var results []*entity.UsageLog
	if err = cursor.All(context.TODO(), &results); err != nil {
		err = errors.Wrap(err, "failed to mapping results data")
		tracer.AddSpanError(span, err)
		return []*entity.UsageLog{}, err
	}

	return results, nil
}

func (repo *logUsageRepositoryImpl) GetById(ctx context.Context, id string) (*entity.UsageLog, error) {
	ctx, span := tracer.NewSpan(ctx, "LogUsageRepositoryImpl.GetById", nil)
	defer span.End()

	var usageLog entity.UsageLog
	coll := repo.db.Collection("log_usage")
	if err := coll.FindOne(ctx, bson.D{
		{"_id", id},
	}).Decode(&usageLog); err != nil {
		err = errors.Wrap(err, fmt.Sprintf("failed to get data with id: %v", id))
		tracer.AddSpanError(span, err)
		return nil, err
	}

	return &usageLog, nil
}

func (repo *logUsageRepositoryImpl) Store(ctx context.Context, data map[string]any) (err error) {
	ctx, span := tracer.NewSpan(ctx, "LogUsageRepositoryImpl.Store", nil)
	defer span.End()

	data["_id"] = uuid.New().String()

	coll := repo.db.Collection("log_usage")
	_, err = coll.InsertOne(ctx, data)
	if err != nil {
		tracer.AddSpanError(span, err)
		return err
	}

	return nil
}
