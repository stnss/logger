package main

import (
	"gitlab.com/djelasid/neo-logger/cmd"
)

func main() {
	cmd.Start()
}
